require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
	def setup
			@user = users(:michael)
	end

  test "layout links" do
		get root_path
		assert_template	'static_pages/home'
		assert_select	"a[href=?]", root_path, count:2
		assert_select	"a[href=?]", help_path
		assert_select	"a[href=?]", login_path
		assert_select	"a[href=?]", about_path
		assert_select	"a[href=?]", contact_path
		get contact_path
		assert_select "title", full_title("Contact")
		get signup_path
		assert_select "title", full_title("Signup")
	end

  test "layout links for login" do
		log_in_as(@user)
		get root_path
		assert_template	'static_pages/home'
		assert_select	"a[href=?]", root_path, count:2
		assert_select	"a[href=?]", help_path
		assert_select	"a[href=?]", users_path
		assert_select	"a[href=?]", about_path
		assert_select	"a[href=?]", contact_path
		get contact_path
		assert_select "title", full_title("Contact")
		get signup_path
		assert_select "title", full_title("Signup")
	end

	test "home display" do
		log_in_as(@user)
		get root_path
		assert_template 'static_pages/home'
		assert_select 'title', full_title()
		assert_select 'h1', text: @user.name
		assert_select 'a>img.gravatar'
		assert_select "strong[id=?]",'following',text:@user.following.count.to_s
		assert_select "strong[id=?]",'followers',text:@user.followers.count.to_s
		assert_match @user.microposts.count.to_s, response.body
		assert_select 'div.pagination'
#14.3.2のfeed変更により通らなくなる
#			@user.microposts.paginate(page: 1).each do | micropost |
#				assert_match micropost.content, response.body
#			end
	end
end
