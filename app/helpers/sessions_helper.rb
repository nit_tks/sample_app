module SessionsHelper

	def log_in(user)
		session[:user_id] = user.id
	end

	# ユーザのセッションを永続的にする
	def remember(user)
		user.remember
		cookies.permanent.signed[:user_id] = user.id
		cookies.permanent[:remember_token] = user.remember_token
	end

	def current_user?(user)
		user == current_user
	end

	# 記憶トークンcookieに対応するユーザを返す
	def current_user
#		debugger
#		logger.debug("@current_user.nil?:#{@current_user.nil?}")		# <- ok
		if (user_id = session[:user_id])
			@current_user ||= User.find_by(id: user_id)
#			logger.debug("@current_user.nil?:#{@current_user.nil?}")		# <- ok
		elsif (user_id = cookies.signed[:user_id])
#			raise
			user = User.find_by(id:user_id)
			logger.debug("@current_user.nil?:#{@current_user.nil?}")		# <- ok
			if user && user.authenticated?(:remember , cookies[:remember_token])
				log_in user
				@current_user = user
			end
		else
#			logger.debug("@current_user.nil?:#{@current_user.nil?}")		# <- err
		end
#		logger.debug("end def current_user")													# <- err
	end

	def logged_in?
		!current_user.nil?
	end

	# 永続的セッションを破棄する
	def forget(user)
		user.forget
		cookies.delete(:user_id)
		cookies.delete(:remember_token)
	end

	def log_out
#		debugger
		forget(current_user)
		session.delete(:user_id)
#		session[:user_id] = nil
		@current_user=nil
	end

	# 記憶したURL(もしくはデフォルト値)にリダイレクト
	def redirect_back_or(default)
		redirect_to(session[:forwarding_url] || default)
		session.delete(:forwarding_url)
	end

	# アクセスしようとしたURLを保存
	def store_location
		session[:forwarding_url] = request.original_url if request.get?
	end
end
